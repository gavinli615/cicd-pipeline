FROM ubuntu:16.04
ENV HTTP_PROXY http://fastweb.on.bell.ca:8083
ENV HTTPS_PROXY http://fastweb.on.bell.ca:8083
 
# Install prepare infrastructure
RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get -y install openjdk-8-jdk wget 
    #mkdir /usr/local/tomcat
    #wget http://www-us.apache.org/dist/tomcat/tomcat-8/v8.5.16/bin/apache-tomcat-8.5.16.tar.gz -O /tmp/tomcat.tar.gz
    #cd /tmp && tar xvfz tomcat.tar.gz
    #cp -Rv /tmp/apache-tomcat-8.5.16/* /usr/local/tomcat/
   
# Prepare environment
ENV CATALINA_HOME /usr/local/tomcat
ENV PATH $PATH:$JAVA_HOME/bin:$CATALINA_HOME/bin:$CATALINA_HOME

# Install Tomcat
 ENV TOMCAT_MAJOR 8
 ENV TOMCAT_VERSION 8.0.36
 

RUN wget -nv http://archive.apache.org/dist/tomcat/tomcat-${TOMCAT_MAJOR}/v${TOMCAT_VERSION}/bin/apache-tomcat-${TOMCAT_VERSION}.tar.gz \
    -O /opt/apache-tomcat-${TOMCAT_VERSION}.tar.gz && \
    echo "Checking file integrity..." && \
    tar xf /opt/apache-tomcat-${TOMCAT_VERSION}.tar.gz && \
    rm  /opt/apache-tomcat-${TOMCAT_VERSION}.tar.gz && \
    mv apache-tomcat-${TOMCAT_VERSION} ${CATALINA_HOME} && \
    chmod +x ${CATALINA_HOME}/bin/*sh

# Tomcat scripts setup
ADD entrypoint.sh/ ${CATALINA_HOME}/
RUN chmod +x ${CATALINA_HOME}/*.sh


WORKDIR  /home/pli1

ADD /helloWorld.war  /usr/local/tomcat/webapps/helloWorld.war

RUN mkdir -p /home/pli1

#ADD /ScriptLauncher.properties  /home/pli1/ScriptLauncher.properties

CMD ["entrypoint.sh"]
